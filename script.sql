-- crie um script que gera 8 entidades simulando uma loja virtual
create table cliente (
    id serial primary key,
    nome varchar(100),
    email varchar(100),
    senha varchar(100)
);


create table produto (
    id serial primary key,
    nome varchar(100),
    descricao text,
    preco numeric(10,2)
);


create table categoria (
    id serial primary key,
    nome varchar(100)
);


create table produto_categoria (
    id serial primary key,
    id_produto integer references produto(id),
    id_categoria integer references categoria(id)
);


create table pedido (
    id serial primary key,
    id_cliente integer references cliente(id),
    data date
);


create table pedido_produto (
    id serial primary key,
    id_pedido integer references pedido(id),
    id_produto integer references produto(id),
    quantidade integer
);


create table endereco (
    id serial primary key,
    id_cliente integer references cliente(id),
    rua varchar(100),
    numero integer,
    bairro varchar(100),
    cidade varchar(100),
    estado varchar(100),
    cep varchar(100)
);


-- Path: loja.sql
-- crie um script que insere 5 registros em cada uma das entidades
insert into cliente (nome, email, senha) values
    ('Cliente 1', 'cliente@email.com', '123456'),
    ('Cliente 2', 'cliente2@email.com', '123456'),
    ('Cliente 3', 'cliente3@email.com', '123456'),
    ('Cliente 4', 'cliente4@email.com', '123456'),
    ('Cliente 5', 'cliente5@email.com', '123456'),
    ('Cliente 6', 'cliente6@email.com', '123456');


insert into produto (nome, descricao, preco) values
    ('Produto 1', 'Descrição do produto 1', 100.00),
    ('Produto 2', 'Descrição do produto 2', 200.00),
    ('Produto 3', 'Descrição do produto 3', 300.00),
    ('Produto 4', 'Descrição do produto 4', 400.00),
    ('Produto 5', 'Descrição do produto 5', 500.00),
    ('Produto 6', 'Descrição do produto 6', 600.00);


insert into categoria (nome) values
    ('Categoria 1'),
    ('Categoria 2'),
    ('Categoria 3'),
    ('Categoria 4'),
    ('Categoria 5'),
    ('Categoria 6');


insert into produto_categoria (id_produto, id_categoria) values
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6);


insert into pedido (id_cliente, data) values
    (1, '2021-01-01'),
    (2, '2021-01-02'),
    (3, '2021-01-03'),
    (4, '2021-01-04'),
    (5, '2021-01-05'),
    (6, '2021-01-06');


insert into pedido_produto (id_pedido, id_produto, quantidade) values
    (1, 1, 1),
    (2, 2, 2),
    (3, 3, 3),
    (4, 4, 4),
    (5, 5, 5),
    (6, 6, 6);


insert into endereco (id_cliente, rua, numero, bairro, cidade, estado, cep) values
    (1, 'Rua 1', 1, 'Bairro 1', 'Cidade 1', 'Estado 1', '00000-000'),
    (2, 'Rua 2', 2, 'Bairro 2', 'Cidade 2', 'Estado 2', '11111-111'),
    (3, 'Rua 3', 3, 'Bairro 3', 'Cidade 3', 'Estado 3', '22222-222'),
    (4, 'Rua 4', 4, 'Bairro 4', 'Cidade 4', 'Estado 4', '33333-333'),
    (5, 'Rua 5', 5, 'Bairro 5', 'Cidade 5', 'Estado 5', '44444-444'),
    (6, 'Rua 6', 6, 'Bairro 6', 'Cidade 6', 'Estado 6', '55555-555');

